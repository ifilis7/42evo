#include "../include/philo.h"

int	main(int argc, char **argv)
{
	// pthread_t newthread;
	t_master *master;
	if (argc == 5 || argc == 6)
	{
		printf("Printoff! \n");
		args_are_valid(argv);
		init_master(&master, argv);

		printf("master->philo_nb: %u \n", master->philo_nb);
		printf("master->time_to_die: %ld \n", master->time_to_die);
		printf("master->time_to_eat: %ld \n", master->time_to_eat);
		printf("master->time_to_sleep: %ld \n", master->time_to_sleep);
		printf("master->repeat_time: %d \n", master->repeat_time);
	}
	else
		msg_error(ERRARG);
}