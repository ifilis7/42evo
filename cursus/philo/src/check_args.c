#include "../include/philo.h"

// void	ft_putnbr(int nb)
// {
// 	if (nb < 0)
// 	{
// 		ft_putchar('-');
// 		nb = -nb;
// 	}
// 	if (nb >= 10)
// 	{
// 		ft_putnbr(nb / 10);
// 		ft_putchar((nb % 10) + '0');
// 	}
// 	if (nb < 10)
// 	{
// 		ft_putchar((nb % 10) + '0');
// 	}
// }

void	args_are_valid(char **argv)
{
	int	i;
	int	j;

	i = 1;
	while (argv[i])
	{
		j = 0;
		while (argv[i][j])
		{
			if (!(argv[i][j] >= '0' && argv[i][j] <= '9'))
				msg_error(ERRARG);
			j++;
		}
		i++;
	}
}

