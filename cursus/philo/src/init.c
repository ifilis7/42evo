#include "../include/philo.h"

static void	find_best_chops(t_philo *philo)
{
	philo->chop.left = philo->id;
	philo->chop.right = (philo->id + 1) % philo->master.philo_nb;
	if (philo->id % 2)
	{
		philo->chop.left = (philo->id + 1) % philo->master.philo_nb;
		philo->chop.right = philo->id;
	}
}

t_philo	*init_philos(t_master **master)
{
	t_philo			**philos;
	unsigned int	i;

	philos = malloc(sizeof(t_philo) * (*master)->philo_nb);
	if (!philos)
		msg_error(ERRCAL);
	i = 0;
	while (i < (*master)->philo_nb)
	{
		philos[i] = malloc(sizeof(t_philo) * 1);
		if (!philos[i])
			msg_error(ERRCAL);
		if (pthread_mutex_init(&philos[i]->eating_lock, 0) != 0)
			msg_error(ERRCAL);
		philos[i]->master = *(*master);
		philos[i]->id = i;
		philos[i]->times_ate = 0;
		printf("philos[i]->id: %d \n", philos[i]->id);
		find_best_chops(philos[i]);
		i++;
	}
	return (*philos);
}

void	init_master(t_master **master, char **argv)
{
	(*master) = malloc(sizeof(t_master) * 1);
	if (!master)
		msg_error(ERRCAL);
	(*master)->philo_nb = ft_atol(argv[1]);
	(*master)->time_to_die = ft_atol(argv[2]);
	(*master)->time_to_eat = ft_atol(argv[3]);
	(*master)->time_to_sleep = ft_atol(argv[4]);
	(*master)->repeat_time = 1;
	if (argv[5])
		(*master)->repeat_time = ft_atol(argv[5]);
	(*master)->philos = init_philos(master);
	if (!(*master)->philos)
		msg_error(ERRCAL);
}
