/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   philo.h                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: hsaadi <hsaadi@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/09/14 08:22:14 by hsaadi            #+#    #+#             */
/*   Updated: 2022/09/19 20:35:56 by hsaadi           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef PHILO_H
# define PHILO_H

/* ***** INCLUDES ***** */
# include <limits.h>
# include <stdbool.h>
# include <stdint.h>
# include <stdio.h>
# include <stdlib.h>
# include <unistd.h>

// For time management
# include <sys/time.h>

// For threading
# include <pthread.h>

/* ***** MACROS ***** */
# define ERRCAL "You've got Calloc problems!"
# define ERROR "Error!"
# define ERRARG "Error! Only :int: args accepted \n"
# define ERRMAL "Error! Somthing went wrong while trying to malloc\n"

// #define MAX_LIFE
// #define THINK_T
// #define EATING_T
// #define DEATH_NOTE

/* *************** ENUMS *************** */
typedef enum e_bool
{
	False,
	True
}					t_bool;

typedef enum t_state
{
	DEAD,
	ASLEEP,
	EATING,
	THINKING,
	GOT_FORK_1,
	GOT_FORK_2
}					t_state;

/* *************** STRUCTS *************** */
typedef struct s_chop
{
	unsigned int	left;
	unsigned int	right;
}					t_chop;

typedef struct s_master
{
	unsigned int	philo_nb;
	time_t			time_to_eat;
	time_t			time_to_die;
	time_t			time_to_sleep;
	unsigned int	repeat_time;
	struct s_philo	*philos;
}					t_master;

typedef struct s_philo
{
	pthread_t		thread;
	unsigned int	id;
	unsigned int	status;
	time_t			last_meal;
	unsigned int	times_ate;
	pthread_mutex_t	eating_lock;
	t_chop			chop;
	t_master		master;
}					t_philo;

/* *************** FUNCTIONS *************** */

/* ***** LOGS.c ***** */
void				msg_error(char *str);

/* ***** UTILS.c ***** */
int					ft_strlen(char *str);
unsigned int		ft_atol(const char *str);
// void				fru(char **str);

/* ***** CHECK_ARGS.c ***** */
void				args_are_valid(char **argv);
// t_input	*init_input(t_input *input);

/* ***** INIT.c ***** */
void				init_master(t_master **input, char **argv);

#endif