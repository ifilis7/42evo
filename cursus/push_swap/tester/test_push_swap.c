#include "../include/push_swap.h"
#include "../src/check_args.c"
#include "../src/logs.c"
#include "../src/stack_ops.c"
#include "../src/tools.c"
#include "../src/utils.c"
#include "../src/allowed_ops.c"
#include "../src/sorting_small.c"
#include "include/acutest.h"

void	printf_space(void)
{
	TEST_CHECK_(printf("\n") == '\n', "printf(%s)==%d", "\n", '\n');
}

/*  *-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* UTILS.C *-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*  */

void	test_ints_count(void)
{
	TEST_CHECK_(ints_count(ft_split("1 3 6", ' ')) == 3, "ints_count(%p)==%d",
			ft_split("1 3 6", ' '), 3);
	TEST_CHECK_(ints_count(ft_split("1 3 6 9 5", ' ')) == 5,
			"ints_count(%p)==%d", ft_split("1 3 6 9 5", ' '), 3);
	TEST_CHECK_(ints_count(ft_split("", ' ')) == 0, "ints_count(%p)==%d",
			ft_split("", ' '), 0);
}

void	test_indexing(void)
{
	char	*str;

	str = "1 2 3 4 5 6 7 8";
	TEST_CHECK_(indexing(1, ft_split(str, ' ')) == 0, "indexing(%d, %p)==%d", 1,
			ft_split(str, ' '), 0);
	// TEST_CHECK_(indexing(2, ft_split(str, ' ')) == 1, "indexing(%d, %p)==%d", 1,
	// 		ft_split(str, ' '), 1);
	// TEST_CHECK_(indexing(6, ft_split(str, ' ')) == 5, "indexing(%d, %p)==%d", 1,
	// 		ft_split(str, ' '), 5);
}

/*  *-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* TOOLS.C *-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*  */
void	test_ft_isnum_space(void)
{
	TEST_CHECK_(ft_isnum_space('a') == 0, "ft_isnum_space(%c)==%d", 'a', 0);
	TEST_CHECK_(ft_isnum_space('c') == 0, "ft_isnum_space(%c)==%d", 'c', 0);
	TEST_CHECK_(ft_isnum_space('1') == 1, "ft_isnum_space(%c)==%d", '1', 1);
	TEST_CHECK_(ft_isnum_space('9') == 1, "ft_isnum_space(%c)==%d", '9', 1);
	TEST_CHECK_(ft_isnum_space(' ') == 1, "ft_isnum_space(%c)==%d", ' ', 1);
	TEST_CHECK_(ft_isnum_space('-') == 1, "ft_isnum_space(%c)==%d", '-', 1);
}

void	test_ft_min(void)
{
	TEST_CHECK_(ft_min(2, 4) == 2, "ft_min(%d, %d)==%d", 2, 4, 2);
	TEST_CHECK_(ft_min(3, 1) == 1, "ft_min(%d, %d)==%d", 3, 1, 1);
	TEST_CHECK_(ft_min(9, -5) == -5, "ft_min(%d, %d)==%d", 9, -5, -5);
	TEST_CHECK_(ft_min(3, 3) == 3, "ft_min(%d, %d)==%d", 3, 3, 3);
}

// void	test_ft_max(void)
// {
// 	t_stack *stack = calloc(sizeof(t_stack), 1);
// 	TEST_ASSERT(stack != NULL);
// 	push(&stack, new_val(2, 0));

// 	TEST_CHECK_(stack->max == 2, "stack->max%d==%d", stack->max, 2);
// 	TEST_CHECK_(ft_max(2, stack) == 2, "ft_max(%d, %p)==%d", 2, stack, 2);

// 	push(&stack, new_val(4, 1));
// 	TEST_CHECK_(stack->max == 4, "stack->max%d==%d", stack->max, 4);
// 	TEST_CHECK_(ft_max(4, stack) == 4, "ft_max(%d, %p)==%d", 4, stack, 4);

// 	push(&stack, new_val(9, 2));
// 	TEST_CHECK_(stack->max == 9, "stack->max%d==%d", stack->max, 9);
// 	TEST_CHECK_(ft_max(5, stack) == stack->max, "ft_max(%d, %p)==%d", 5, stack, stack->max);

// 	pop(&stack);
// 	TEST_CHECK_(stack->max == 4, "stack->max%d==%d", stack->max, 4);
// 	TEST_CHECK_(ft_max(5, stack) == 5, "ft_max(%d, %p)==%d", 5, stack, 5);

// 	pop(&stack);
// 	TEST_CHECK_(stack->max == 2, "stack->max%d==%d", stack->max, 2);
// 	TEST_CHECK_(ft_max(1, stack) == stack->max, "ft_max(%d, %p)==%d", 1, stack, stack->max);

// 	pop(&stack);
// 	TEST_CHECK_(stack->node == NULL, "stack->max%p==%p", stack, NULL);
	
// }

/*  *-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* CHECK_ARGS.C *-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*  */
void	test_init_stacks(void)
{
	t_stack	*astack;
	t_stack	*bstack;

	init_stacks(&astack, &bstack);
	TEST_CHECK_(astack != NULL, "astack%p == %p", astack, NULL);
	TEST_CHECK_(bstack != NULL, "bstack%p == %p", bstack, NULL);
}

void	test_just_nums(void)
{
	char	*str;
	char	*str1;

	str = "1 2 3 4 5";
	str1 = "a 2 3 4 z";
	TEST_CHECK_(just_nums(str) == True, "just nums %s == %d", str, True);
	TEST_CHECK_(just_nums(str1) == False, "just nums %s == %d", str1, False);
}

void	test_no_duplicates(void)
{
	t_node	*node;

	node = calloc(sizeof(t_node), 1);
	node->val = 2;
	TEST_CHECK_(no_duplicates(node, 1) == True, "no_duplicates(%p, %d)==%d",
			node, 1, True);
	TEST_CHECK_(no_duplicates(node, 3) == True, "no_duplicates(%p, %d)==%d",
			node, 3, True);
}

void	test_new_val(void)
{
	t_node	*node;
	t_node	*node1;

	node = new_val(1, 0);
	node1 = new_val(2, 0);
	TEST_CHECK_(node->val == 1, "node%p node->val: %d", node, 1);
	TEST_CHECK_(node->index == 0, "node%p node->index: %d", node, 0);
	TEST_CHECK_(node1->val == 2, "node1%p node1->val: %d", node1, 2);
}

void	test_store_in_stack(void)
{
	t_stack	*astack;
	t_stack	*bstack;
	char	*args;

	args = "1 2 3 4 5 6 7";
	store_in_stack(&astack, args, &bstack);
	TEST_CHECK_(astack->node != NULL, "node%p node->val: %p", astack->node, NULL);
	TEST_CHECK_(astack->size == 7, "node%p node->val: %d", astack, 7);
	TEST_CHECK_(astack->min == 1, "node%p node->val: %d", astack, 1);
	TEST_CHECK_(astack->max == 7, "node%d node->val: %d", astack->max, 1);
	// TODO Add the tests below to the LIST
	// TEST_CHECK_(astack->node->val == 1, "node%d node->val: %d", astack->node->index, 1);
	// TEST_CHECK_(astack->node->index == 0, "node%d node->val: %d", astack->node->index, 0);
	// TEST_CHECK_(astack->node->next->val == 2, "node%d node->val: %d", astack->node->next->val, 2);
	// TEST_CHECK_(astack->node->next->index == 2, "node%d node->val: %d", astack->node->next->index, 1);
	// TEST_CHECK_(last_node(astack)->val == 7, "node%d node->val: %d", last_node(astack)->val, 7);
	// TEST_CHECK_(last_node(astack)->index == 90, "node%d node->val: %d", last_node(astack)->index, 1);
}

/*  *-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* STACK_OPS.C *-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*  */
void	test_push(void)
{
	t_stack *stack = ft_calloc(sizeof(t_stack), 1);
	push(&stack, new_val(3, 0));
	TEST_CHECK_(stack->size == 1, "node%p node->size: %d", stack, 1);
	TEST_CHECK_(stack->min == 3, "node%p node->val: %d", stack, 3);
	TEST_CHECK_(stack->max == 3, "node%p node->val: %d", stack, 3);
	TEST_CHECK_(stack->node->val == 3, "node%p node->next->val: %d", stack, 3);
	TEST_CHECK_(stack->node->next == NULL, "node%p node->val: %p", stack, NULL);
	TEST_CHECK_(stack->node->prev == NULL, "node%p node->val: %p", stack, NULL);
	
	push(&stack, new_val(1, 1));
	TEST_CHECK_(stack->size == 2, "node%p node->val: %d", stack, 2);
	TEST_CHECK_(stack->min == 1, "node%p node->val: %d", stack, 1);
	TEST_CHECK_(stack->max == 3, "node%p node->val: %d", stack, 3);
	TEST_CHECK_(stack->node->val == 1, "node%p node->val: %d", stack, 1);
	TEST_CHECK_(stack->node->next->val == 3, "node%p node->next->val: %d", stack, 3);
	TEST_CHECK_(stack->node->next->prev->val == 1, "node%p node->val: %d", stack, 1);
	TEST_CHECK_(stack->node->prev == NULL, "node%p node->val: %p", stack, NULL);
	TEST_CHECK_(stack->node->next->next == NULL, "node%p node->val: %p", stack, NULL);
	// TEST_CHECK_(stack->node->next == NULL, "node%p node->val: %p", stack, NULL);
	
	push(&stack, new_val(7, 2));
	TEST_CHECK_(stack->size == 3, "node%p node->size: %d", stack, 3);
	TEST_CHECK_(stack->min == 1, "node%p node->val: %d", stack, 1);
	TEST_CHECK_(stack->max == 7, "node%p node->val: %d", stack, 7);
	TEST_CHECK_(stack->node->val == 7, "node%p node->next->val: %d", stack, 7);
	TEST_CHECK_(stack->node->next->val == 1, "node%p node->next->val: %d", stack, 1);
	TEST_CHECK_(stack->node->next->prev->val == 7, "node%p node->val: %d", stack, 7);
	TEST_CHECK_(stack->node->next->next->val == 3, "node%p node->val: %d", stack, 3);
	TEST_CHECK_(stack->node->prev == NULL, "node%p node->val: %p", stack, NULL);
}

void	test_pop(void)
{
	t_stack *stack = ft_calloc(sizeof(t_stack), 1);;
	push(&stack, new_val(1, 0));
	push(&stack, new_val(3, 1));
	push(&stack, new_val(5, 2));
	TEST_CHECK_(stack->size == 3, "node%p node->val: %d", stack, 3);
	TEST_CHECK_(stack->min == 1, "node%p node->val: %d", stack, 1);
	TEST_CHECK_(stack->max == 5, "node%p node->val: %d", stack, 5);
	TEST_CHECK_(stack->node->val == 5, "node%p node->val: %d", stack, 5);
	TEST_CHECK_(stack->node->next->val == 3, "node%p node->next->val: %d", stack, 3);
	TEST_CHECK_(stack->node->next->next->val == 1, "node%p node->next->val: %d", stack, 1);
	TEST_CHECK_(stack->node->next->prev->val == 5, "node%p node->val: %d", stack, 5);
	TEST_CHECK_(stack->node->prev == NULL, "node%p node->val: %p", stack, NULL);
	TEST_CHECK_(stack->node->next->next->next == NULL, "node%p node->val: %p", stack, NULL);
	pop(&stack);
	TEST_CHECK_(stack->size == 2, "node%p node->val: %d", stack, 2);
	TEST_CHECK_(stack->min == 1, "node%p node->val: %d", stack, 1);
	TEST_CHECK_(stack->max == 3, "node%p node->val: %d", stack, 3);
	TEST_CHECK_(stack->node->val == 3, "node%p node->val: %d", stack, 3);
	TEST_CHECK_(stack->node->next->val == 1, "node%p node->next->val: %d", stack, 1);
	TEST_CHECK_(stack->node->next->prev->val == 3, "node%p node->val: %d", stack, 3);
	TEST_CHECK_(stack->node->prev == NULL, "node%p node->val: %p", stack, NULL);
	TEST_CHECK_(stack->node->next->next == NULL, "node%p node->val: %p", stack, NULL);
	pop(&stack);
	TEST_CHECK_(stack->size == 1, "node%p node->val: %d", stack, 1);
	TEST_CHECK_(stack->min == 1, "node%p node->val: %d", stack, 1);
	TEST_CHECK_(stack->max == 1, "node%p node->val: %d", stack, 1);
	TEST_CHECK_(stack->node->val == 1, "node%p node->val: %d", stack, 1);
	TEST_CHECK_(stack->node->prev == NULL, "node%p node->val: %p", stack, NULL);
	TEST_CHECK_(stack->node->next == NULL, "node%p node->val: %p", stack, NULL);
	pop(&stack);
	TEST_CHECK_(stack->size == 0, "node%p node->val: %d", stack, 0);
	TEST_CHECK_(stack->min == 1, "node%p node->val: %d", stack, 1); //TODO should be 0
	TEST_CHECK_(stack->max == 0, "node%p node->val: %d", stack, 0);
	TEST_CHECK_(stack->node == NULL, "node%p node->val: %p", stack->node, NULL);
}

void test_last_node(void)
{
	t_stack *stack = ft_calloc(sizeof(t_stack), 1);
	push(&stack, new_val(1, 0));
	TEST_CHECK_(last_node(stack)->val == 1, "last_node%p last_node->val: %d", last_node(stack), 1);
	push(&stack, new_val(3, 1));
	TEST_CHECK_(last_node(stack)->val == 1, "last_node%p last_node->val: %d", last_node(stack), 1);
}

/*  *-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* ALLOWED_OPS.C *-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*  */
void test_swap_val()
{
	t_stack *stack = ft_calloc(sizeof(t_stack), 1);
	int turns = 0;
	push(&stack, new_val(1, 0));
	push(&stack, new_val(3, 1));
	push(&stack, new_val(5, 2));
	TEST_CHECK_(stack->size == 3, "node%p node->val: %d", stack, 3);
	TEST_CHECK_(stack->min == 1, "node%p node->val: %d", stack, 1);
	TEST_CHECK_(stack->max == 5, "node%p node->val: %d", stack, 5);
	TEST_CHECK_(stack->node->val == 5, "node%p node->val: %d", stack, 5);
	TEST_CHECK_(stack->node->prev == NULL, "node%p node->val: %p", stack, NULL);
	TEST_CHECK_(stack->node->next->val == 3, "node%p node->val: %d", stack, 3);
	TEST_CHECK_(stack->node->next->prev->val == 5, "node%p node->val: %p", stack, NULL);
	swap_val(&stack, &turns);
	TEST_CHECK_(stack->node->val == 3, "node%p node->val: %d", stack, 3);
	TEST_CHECK_(stack->node->prev == NULL, "node%p node->val: %p", stack, NULL);
	TEST_CHECK_(stack->node->next->val == 5, "node%p node->val: %d", stack, 5);
	TEST_CHECK_(stack->node->next->prev->val == 3, "node%p node->val: %d", stack, 3);
	pop(&stack);
	swap_val(&stack, &turns);
	TEST_CHECK_(stack->node->val == 1, "node%p node->val: %d", stack, 1);
	TEST_CHECK_(stack->node->prev == NULL, "node%p node->val: %p", stack, NULL);
	TEST_CHECK_(stack->node->next->val == 5, "node%p node->val: %d", stack, 5);
	TEST_CHECK_(stack->node->next->prev->val == 1, "node%p node->val: %d", stack, 1);
	TEST_CHECK_(stack->node->next->next == NULL, "node%p node->val: %p", stack, NULL);
}

void test_push_to_stack(void)
{
	t_stack *astack;
	t_stack *bstack;
	int turns = 0;
	init_stacks(&astack, &bstack);
	push(&astack, new_val(1, 0));
	push(&astack, new_val(3, 1));
	push(&astack, new_val(5, 2));
	TEST_CHECK_(astack->size == 3, "node%p node->val: %d", astack, 3);
	TEST_CHECK_(astack->min == 1, "node%p node->val: %d", astack, 1);
	TEST_CHECK_(astack->max == 5, "node%p node->val: %d", astack, 5);
	TEST_CHECK_(astack->node->val == 5, "node%d node->val: %d", astack->node->val, 5);
	TEST_CHECK_(astack->node->prev == NULL, "node%p node->val: %p", astack->node->prev, NULL);
	TEST_CHECK_(last_node(astack)->val == 1, "node%d node->val: %d", last_node(astack)->val, 1);
	TEST_CHECK_(last_node(astack)->next == NULL, "node%p node->val: %p", last_node(astack)->next, NULL);

	push_to_stack(&astack, &bstack, &turns);
	TEST_CHECK_(astack->size == 2, "node%p node->val: %d", astack, 2);
	TEST_CHECK_(astack->min == 1, "node%p node->val: %d", astack, 1);
	TEST_CHECK_(astack->max == 3, "node%p node->val: %d", astack, 3);
	TEST_CHECK_(astack->node->val == 3, "node%d node->val: %d", astack->node->val, 3);
	TEST_CHECK_(last_node(astack)->val == 1, "node%d node->val: %d", last_node(astack)->val, 1);
	TEST_CHECK_(astack->node->prev == NULL, "node%p node->val: %p", astack->node->prev, NULL);

	TEST_CHECK_(bstack->size == 1, "node%p node->val: %d", bstack, 1);
	TEST_CHECK_(bstack->min == 5, "node%p node->val: %d", bstack, 5);
	TEST_CHECK_(bstack->max == 5, "node%p node->val: %d", bstack, 5);
	TEST_CHECK_(bstack->node->val == 5, "node%d node->val: %d", bstack->node->val, 5);
	TEST_CHECK_(last_node(bstack)->val == 5, "node%d node->val: %d", last_node(bstack)->val, 5);
	TEST_CHECK_(last_node(bstack)->next == NULL, "node%p node->val: %p", last_node(bstack)->next, NULL);
	TEST_CHECK_(bstack->node->prev == NULL, "node%p node->val: %p", bstack->node->prev, NULL);

	push_to_stack(&astack, &bstack, &turns);
	TEST_CHECK_(astack->size == 1, "node%p node->val: %d", astack, 1);
	TEST_CHECK_(astack->min == 1, "node%p node->val: %d", astack, 1);
	TEST_CHECK_(astack->max == 1, "node%p node->val: %d", astack, 1);
	TEST_CHECK_(astack->node->val == 1, "node%d node->val: %d", astack->node->val, 1);
	TEST_CHECK_(last_node(astack)->val == 1, "node%d node->val: %d", last_node(astack)->val, 1);

	TEST_CHECK_(bstack->size == 2, "node%p node->val: %d", bstack, 2);
	TEST_CHECK_(bstack->min == 3, "node%p node->val: %d", bstack, 3);
	TEST_CHECK_(bstack->max == 5, "node%p node->val: %d", bstack, 5);
	TEST_CHECK_(bstack->node->val == 3, "node%d node->val: %d", bstack->node->val, 3);
	TEST_CHECK_(bstack->node->prev == NULL, "node%p node->val: %p", bstack->node, NULL);
	TEST_CHECK_(bstack->node->next->val == 5, "node%d node->val: %d", bstack->node->next->val, 5);
	TEST_CHECK_(last_node(bstack)->val == 5, "node%d node->val: %d", last_node(bstack)->val, 5);
	TEST_CHECK_(last_node(bstack)->prev->val == 3, "node%d node->val: %d", last_node(bstack)->val, 3);
	TEST_CHECK_(last_node(bstack)->next == NULL, "node%p node->val: %p", last_node(bstack)->next, NULL);
	// print_stack(astack);
	// print_stack(bstack);
	push_to_stack(&bstack, &astack, &turns);
	print_stack(astack);
	print_stack(bstack);
	// pop(&astack);
	// pop(&bstack);
	// pop(&bstack);
	// TEST_CHECK_(astack->node == NULL, "node%p node->val: %p", astack->node, NULL);
	// TEST_CHECK_(bstack->node == NULL, "node%p node->val: %p", bstack->node, NULL);
}

void test_rotate(void)
{
	t_stack *astack = ft_calloc(sizeof(t_stack), 1);
	int turns = 0;
	push(&astack, new_val(1, 0));
	push(&astack, new_val(7, 3));
	push(&astack, new_val(3, 1));
	push(&astack, new_val(5, 2));
	TEST_CHECK_(astack->size == 4, "node%p node->val: %d", astack, 4);
	TEST_CHECK_(astack->node->val == 5, "node%d node->val: %d", astack->node->val, 5);
	TEST_CHECK_(astack->node->prev == NULL, "node%p node->val: %p", astack->node->prev, NULL);
	TEST_CHECK_(last_node(astack)->val == 1, "node%d node->val: %d", last_node(astack)->val, 1);
	TEST_CHECK_(last_node(astack)->next == NULL, "node%p == node->val: %p", last_node(astack)->next, NULL);
	rotate(&astack, &turns);

	TEST_CHECK_(astack->size == 4, "node%p node->val: %d", astack, 4);
	TEST_CHECK_(astack->node->val == 3, "node%d node->val: %d", astack->node->val, 3);
	TEST_CHECK_(astack->node->next->val == 7, "node%d node->val: %d", astack->node->val, 7);
	TEST_CHECK_(astack->node->next->prev->val == 3, "node%d node->val: %d", astack->node->next->prev->val, 3);
	TEST_CHECK_(astack->node->prev == NULL, "node%p node->val: %p", astack->node->prev, NULL);
	TEST_CHECK_(last_node(astack)->val == 5, "node%d node->val: %d", last_node(astack)->val, 5);
	TEST_CHECK_(last_node(astack)->prev->val == 1, "node%d node->val: %d", last_node(astack)->prev->val, 1);
	TEST_CHECK_(last_node(astack)->prev->next->val == 5, "node%d node->val: %d", last_node(astack)->prev->next->val, 5);
	TEST_CHECK_(last_node(astack)->next == NULL, "node%p == node->val: %p", last_node(astack)->next, NULL);
	rotate(&astack, &turns);

	TEST_CHECK_(astack->size == 4, "node%p node->val: %d", astack, 4);
	TEST_CHECK_(astack->node->val == 7, "node%d node->val: %d", astack->node->val, 7);
	TEST_CHECK_(astack->node->next->val == 1, "node%d node->val: %d", astack->node->val, 1);
	TEST_CHECK_(astack->node->next->prev->val == 7, "node%d node->val: %d", astack->node->next->prev->val, 7);
	TEST_CHECK_(astack->node->prev == NULL, "node%p node->val: %p", astack->node->prev, NULL);
	TEST_CHECK_(last_node(astack)->val == 3, "node%d node->val: %d", last_node(astack)->val, 3);
	TEST_CHECK_(last_node(astack)->prev->val == 5, "node%d node->val: %d", last_node(astack)->prev->val, 5);
	TEST_CHECK_(last_node(astack)->prev->next->val == 3, "node%d node->val: %d", last_node(astack)->prev->next->val, 3);
	TEST_CHECK_(last_node(astack)->next == NULL, "node%p == node->val: %p", last_node(astack)->next, NULL);
}

void test_reverse_rotate(void)
{
	t_stack *astack = ft_calloc(sizeof(t_stack), 1);
	int turns = 0;
	push(&astack, new_val(1, 0));
	push(&astack, new_val(7, 3));
	push(&astack, new_val(3, 1));
	push(&astack, new_val(5, 2));

	TEST_CHECK_(astack->size == 4, "node%p node->val: %d", astack, 4);
	TEST_CHECK_(astack->node->val == 5, "node%d node->val: %d", astack->node->val, 5);
	TEST_CHECK_(astack->node->prev == NULL, "node%p node->val: %p", astack->node->prev, NULL);
	TEST_CHECK_(last_node(astack)->val == 1, "node%d node->val: %d", last_node(astack)->val, 1);
	TEST_CHECK_(last_node(astack)->next == NULL, "node%p == node->val: %p", last_node(astack)->next, NULL);
	
	rotate_reverse(&astack, &turns);
	TEST_CHECK_(astack->size == 4, "node%p node->val: %d", astack, 4);
	TEST_CHECK_(astack->node->val == 1, "node%d node->val: %d", astack->node->val, 1);
	TEST_CHECK_(astack->node->next->val == 5, "node%d node->val: %d", astack->node->val, 5);
	TEST_CHECK_(astack->node->next->prev->val == 1, "node%d node->val: %d", astack->node->next->prev->val, 1);
	TEST_CHECK_(astack->node->prev == NULL, "node%p node->val: %p", astack->node->prev, NULL);
	TEST_CHECK_(last_node(astack)->val == 7, "node%d node->val: %d", last_node(astack)->val, 7);
	TEST_CHECK_(last_node(astack)->prev->val == 3, "node%d node->val: %d", last_node(astack)->prev->val, 3);
	TEST_CHECK_(last_node(astack)->prev->next->val == 7, "node%d node->val: %d", last_node(astack)->prev->next->val, 7);
	TEST_CHECK_(last_node(astack)->next == NULL, "node%p == node->val: %p", last_node(astack)->next, NULL);

	rotate_reverse(&astack, &turns);
	TEST_CHECK_(astack->size == 4, "node%p node->val: %d", astack, 4);
	TEST_CHECK_(astack->node->val == 7, "node%d node->val: %d", astack->node->val, 7);
	TEST_CHECK_(astack->node->next->val == 1, "node%d node->val: %d", astack->node->val, 1);
	TEST_CHECK_(astack->node->next->prev->val == 7, "node%d node->val: %d", astack->node->next->prev->val, 7);
	TEST_CHECK_(astack->node->prev == NULL, "node%p node->val: %p", astack->node->prev, NULL);
	TEST_CHECK_(last_node(astack)->val == 3, "node%d node->val: %d", last_node(astack)->val, 3);
	TEST_CHECK_(last_node(astack)->prev->val == 5, "node%d node->val: %d", last_node(astack)->prev->val, 5);
	TEST_CHECK_(last_node(astack)->prev->next->val == 3, "node%d node->val: %d", last_node(astack)->prev->next->val, 3);
	TEST_CHECK_(last_node(astack)->next == NULL, "node%p == node->val: %p", last_node(astack)->next, NULL);
}

/*  *-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* SORTING_SMALL.C *-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*  */
void test_sorting_three(void)
{
	t_stack *astack = ft_calloc(sizeof(t_stack), 1);
	int turns = 0;
	push(&astack, new_val(3, 2));
	push(&astack, new_val(1, 1));
	push(&astack, new_val(2, 0));
	TEST_CHECK_(astack->size == 3, "node%p node->val: %d", astack, 3);
	TEST_CHECK_(astack->node->val == 2, "node%d node->val: %d", astack->node->val, 2);
	TEST_CHECK_(astack->node->next->val == 1, "node%d node->val: %d", astack->node->next->val, 1);
	TEST_CHECK_(last_node(astack)->val == 3, "node%d node->val: %d", last_node(astack)->val, 3);
	sorting_three(&astack, &turns);
	TEST_CHECK_(astack->size == 3, "node%p node->val: %d", astack, 3);
	TEST_CHECK_(astack->node->val == 1, "node%d node->val: %d", astack->node->val, 1);
	TEST_CHECK_(astack->node->next->val == 2, "node%d node->val: %d", astack->node->next->val, 2);
	TEST_CHECK_(last_node(astack)->val == 3, "node%d node->val: %d", last_node(astack)->val, 3);
	TEST_CHECK_(turns <= 3, "node%d node->val: %d", 3);


	clear_list(&astack);
	turns = 0;
	push(&astack, new_val(1, 1));
	push(&astack, new_val(2, 0));
	push(&astack, new_val(3, 2));
	TEST_CHECK_(astack->size == 3, "node%p node->val: %d", astack, 3);
	TEST_CHECK_(astack->node->val == 3, "node%d node->val: %d", astack->node->val, 3);
	TEST_CHECK_(astack->node->next->val == 2, "node%d node->val: %d", astack->node->next->val, 2);
	TEST_CHECK_(last_node(astack)->val == 1, "node%d node->val: %d", last_node(astack)->val, 1);
	sorting_three(&astack, &turns);
	TEST_CHECK_(astack->size == 3, "node%p node->val: %d", astack, 3);
	TEST_CHECK_(astack->node->val == 1, "node%d node->val: %d", astack->node->val, 1);
	TEST_CHECK_(astack->node->next->val == 2, "node%d node->val: %d", astack->node->next->val, 2);
	TEST_CHECK_(last_node(astack)->val == 3, "node%d node->val: %d", last_node(astack)->val, 3);
	TEST_CHECK_(turns <= 3, "node%d node->val: %d", 3);

	clear_list(&astack);
	push(&astack, new_val(2, 1));
	push(&astack, new_val(1, 0));
	push(&astack, new_val(3, 2));
	TEST_CHECK_(astack->size == 3, "node%p node->val: %d", astack, 3);
	TEST_CHECK_(astack->node->val == 3, "node%d node->val: %d", astack->node->val, 3);
	TEST_CHECK_(astack->node->next->val == 1, "node%d node->val: %d", astack->node->next->val, 1);
	TEST_CHECK_(last_node(astack)->val == 2, "node%d node->val: %d", last_node(astack)->val, 2);
	sorting_three(&astack, &turns);
	TEST_CHECK_(astack->size == 3, "node%p node->val: %d", astack, 3);
	TEST_CHECK_(astack->node->val == 1, "node%d node->val: %d", astack->node->val, 1);
	TEST_CHECK_(astack->node->next->val == 2, "node%d node->val: %d", astack->node->next->val, 2);
	TEST_CHECK_(last_node(astack)->val == 3, "node%d node->val: %d", last_node(astack)->val, 3);

	clear_list(&astack);
	push(&astack, new_val(2, 1));
	push(&astack, new_val(3, 0));
	push(&astack, new_val(1, 2));
	TEST_CHECK_(astack->size == 3, "node%p node->val: %d", astack, 3);
	TEST_CHECK_(astack->node->val == 1, "node%d node->val: %d", astack->node->val, 1);
	TEST_CHECK_(astack->node->next->val == 3, "node%d node->val: %d", astack->node->next->val, 3);
	TEST_CHECK_(last_node(astack)->val == 2, "node%d node->val: %d", last_node(astack)->val, 2);
	sorting_three(&astack, &turns);
	TEST_CHECK_(astack->size == 3, "node%p node->val: %d", astack, 3);
	TEST_CHECK_(astack->node->val == 1, "node%d node->val: %d", astack->node->val, 1);
	TEST_CHECK_(astack->node->next->val == 2, "node%d node->val: %d", astack->node->next->val, 2);
	TEST_CHECK_(last_node(astack)->val == 3, "node%d node->val: %d", last_node(astack)->val, 3);

	clear_list(&astack);
	push(&astack, new_val(1, 2));
	push(&astack, new_val(3, 0));
	push(&astack, new_val(2, 1));
	TEST_CHECK_(astack->size == 3, "node%p node->val: %d", astack, 3);
	TEST_CHECK_(astack->node->val == 2, "node%d node->val: %d", astack->node->val, 2);
	TEST_CHECK_(astack->node->next->val == 3, "node%d node->val: %d", astack->node->next->val, 3);
	TEST_CHECK_(last_node(astack)->val == 1, "node%d node->val: %d", last_node(astack)->val, 1);
	sorting_three(&astack, &turns);
	TEST_CHECK_(astack->size == 3, "node%p node->val: %d", astack, 3);
	TEST_CHECK_(astack->node->val == 1, "node%d node->val: %d", astack->node->val, 1);
	TEST_CHECK_(astack->node->next->val == 2, "node%d node->val: %d", astack->node->next->val, 2);
	TEST_CHECK_(last_node(astack)->val == 3, "node%d node->val: %d", last_node(astack)->val, 3);
}

void test_sorting_five(void)
{
	t_stack *astack;
	t_stack *bstack;
	int turns = 0;
	init_stacks(&astack, &bstack);
	push(&astack, new_val(1, 4));
	push(&astack, new_val(4, 3));
	push(&astack, new_val(2, 2));
	push(&astack, new_val(3, 1));
	push(&astack, new_val(5, 0));
	// print_stack(astack);
	// result = (rand() % (hi_num - low_num)) + low_num;

	TEST_CHECK_(astack->size == 5, "node%p node->val: %d", astack, 5);
	TEST_CHECK_(astack->node->val == 5, "node%d node->val: %d", astack->node->val, 5);
	TEST_CHECK_(astack->node->prev == NULL, "node%p node->val: %p", astack->node->prev, NULL);
	TEST_CHECK_(astack->node->next->val == 3, "node%d node->val: %d", astack->node->next->val, 3);
	TEST_CHECK_(last_node(astack)->val == 1, "node%d node->val: %d", last_node(astack)->val, 1);
	TEST_CHECK_(last_node(astack)->next == NULL, "node%p node->val: %p", last_node(astack)->next, NULL);

	push_to_stack(&astack, &bstack, &turns);
	TEST_CHECK_(astack->size == 4, "node%p node->val: %d", astack, 4);
	TEST_CHECK_(astack->node->val == 3, "node%d node->val: %d", astack->node->val, 3);
	TEST_CHECK_(astack->node->prev == NULL, "node%p node->val: %p", astack->node->prev, NULL);
	TEST_CHECK_(astack->node->next->val == 2, "node%d node->val: %d", astack->node->next->val, 2);
	TEST_CHECK_(last_node(astack)->val == 1, "node%d node->val: %d", last_node(astack)->val, 1);
	TEST_CHECK_(last_node(astack)->next == NULL, "node%p node->val: %p", last_node(astack)->next, NULL);

	TEST_CHECK_(bstack->size ==1, "node%p node->val: %d", astack, 1);
	TEST_CHECK_(bstack->node->val == 5, "node%d node->val: %d", astack->node->val, 5);
	TEST_CHECK_(bstack->node->prev == NULL, "node%p node->val: %p", astack->node->prev, NULL);

	push_to_stack(&astack, &bstack, &turns);
	TEST_CHECK_(astack->size == 3, "node%p node->val: %d", astack, 3);
	TEST_CHECK_(astack->node->val == 2, "node%d node->val: %d", astack->node->val, 2);
	TEST_CHECK_(astack->node->prev == NULL, "node%p node->val: %p", astack->node->prev, NULL);
	TEST_CHECK_(astack->node->next->val == 4, "node%d node->val: %d", astack->node->next->val, 4);
	TEST_CHECK_(last_node(astack)->val == 1, "node%d node->val: %d", last_node(astack)->val, 1);
	TEST_CHECK_(last_node(astack)->next == NULL, "node%p node->val: %p", last_node(astack)->next, NULL);

	TEST_CHECK_(bstack->size ==2, "node%p node->val: %d", astack, 2);
	TEST_CHECK_(bstack->node->val == 3, "node%d node->val: %d", astack->node->val, 3);
	TEST_CHECK_(bstack->node->prev == NULL, "node%p node->val: %p", astack->node->prev, NULL);
	TEST_CHECK_(bstack->node->next->val == 5, "node%d node->val: %d", bstack->node->next->val, 5);
	TEST_CHECK_(bstack->node->next->next == NULL, "node%p node->val: %p", bstack->node->next->next, NULL);
	
	sorting_three(&astack, &turns);
	TEST_ASSERT(astack->size == 3);
	TEST_ASSERT(astack->node->val < astack->node->next->val);
	TEST_ASSERT(last_node(astack)->prev->val < last_node(astack)->val);

	algo_five(&astack, &bstack, &turns);
	TEST_ASSERT(astack->size == 4);
	t_node *temp= astack->node;
	while(temp->next != NULL)
	{
		TEST_ASSERT(temp->val < temp->next->val);
		temp = temp->next;
	}
	
	algo_five(&astack, &bstack, &turns);
	TEST_ASSERT(astack->size == 5);
	temp= astack->node;
	while(temp->next != NULL)
	{
		TEST_ASSERT(temp->val < temp->next->val);
		temp = temp->next;
	}
	
	// clear_list(&astack);
	// turns = 0;
	// push(&astack, new_val(5, 1));
	// push(&astack, new_val(3, 0));
	// push(&astack, new_val(2, 2));
	// push(&astack, new_val(4, 3));
	// push(&astack, new_val(1, 4));
	// TEST_CHECK_(astack->size == 3, "node%p node->val: %d", astack, 3);
	// TEST_CHECK_(astack->node->val == 1, "node%d node->val: %d", astack->node->val, 1);
	// TEST_CHECK_(astack->node->next->val == 2, "node%d node->val: %d", astack->node->next->val, 2);
	// TEST_CHECK_(last_node(astack)->val == 5, "node%d node->val: %d", last_node(astack)->val, 5);
	// sorting_three(&astack, &turns);
	// TEST_CHECK_(astack->size == 3, "node%p node->val: %d", astack, 3);
	// TEST_CHECK_(astack->node->val == 1, "node%d node->val: %d", astack->node->val, 1);
	// TEST_CHECK_(astack->node->next->val == 2, "node%d node->val: %d", astack->node->next->val, 2);
	// TEST_CHECK_(last_node(astack)->val == 3, "node%d node->val: %d", last_node(astack)->val, 3);
	// TEST_CHECK_(turns <= 3, "node%d node->val: %d", 3);

	

}



/*  *-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* TEST_LIST *-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*  */
TEST_LIST = {

	/*  *-*-*-*-*-*-*-*-*-* UTILS.C *-*-*-*-*-*-*-*-*-*  */
	/*  *-*-*-* test_ints_count() *-*-*-*  */
	{"\n    int ints_count(1 3 6 9 5)", test_ints_count},
	{"int ints_count(1 3 6)", test_ints_count},
	{"int ints_count(" ")", test_ints_count},

	/*  *-*-*-* test_indexing() *-*-*-*  */
	{"\n    int indexing(1, &str)", test_indexing},
	{"int indexing(2, &str)", test_indexing},
	{"int indexing(6, &str)", test_indexing},

	/*  *-*-*-*-*-*-*-*-*-* TOOLS.C *-*-*-*-*-*-*-*-*-*  */
	/*  *-*-*-* test_ft_isnum_space() *-*-*-*  */
	{"\n    int ft_isnum_space('a')", test_ft_isnum_space},
	{"int ft_isnum_space('c')", test_ft_isnum_space},
	{"int ft_isnum_space('1')", test_ft_isnum_space},
	{"int ft_isnum_space('9')", test_ft_isnum_space},
	{"int ft_isnum_space(' ')", test_ft_isnum_space},
	{"int ft_isnum_space('-')", test_ft_isnum_space},

	/*  *-*-*-* test_ft_min() *-*-*-*  */
	{"\n    int ft_min(2, 4)", test_ft_min},
	{"int ft_min(3, 1)", test_ft_min},
	{"int ft_min(9, -5)", test_ft_min},
	{"int ft_min(3, 3)", test_ft_min},

	// /*  *-*-*-* test_ft_max() *-*-*-*  */
	// {"\n    Push new_val(1, 0):: stack->max == 4", test_ft_max},
	// {"int ft_max(8, 1)", test_ft_max},
	// {"Push new_val(4, 1):stack->max == 4", test_ft_max},
	// {"int ft_max(5, 4) == 5", test_ft_max},
	// {"Push new_val(9, 2):stack->max == 9", test_ft_max},
	// {"int ft_max(5, 9) == 9", test_ft_max},
	// {"Pop(&stack):stack->max == 9", test_ft_max},
	// {"int ft_max(5, 9) == stack->max", test_ft_max},
	// {"Pop(&stack):stack->max == 2", test_ft_max},
	// {"int ft_max(1, stack) == stack->max", test_ft_max},
	// {"Pop(&stack):stack->node == NULL", test_ft_max},

	// /*  *-*-*-*-*-*-*-*-*-* CHECK_ARGS.C *-*-*-*-*-*-*-*-*-*  */
	// /*  *-*-*-* test_init_stacks() *-*-*-*  */
	// {"\n    void init_stacks() |astack != NULL|", test_init_stacks},
	// {"void init_stacks() |bstack != NULL| ", test_init_stacks},

	// /*  *-*-*-* test_just_nums() *-*-*-*  */
	// {"\n    void just_nums(1 2 3 4 5) == True", test_just_nums},
	// {"void just_nums(a 2 3 4 z) == False", test_just_nums},

	// /*  *-*-*-* test_no_duplicates() *-*-*-*  */
	// {"\n    int no_duplicates(1 3 6 9 5, 1)", test_no_duplicates},
	// {"int no_duplicates(1 3 6 9 5, 3)", test_no_duplicates},

	// /*  *-*-*-* test_new_val() *-*-*-*  */
	// {"\n    t_node new_val(1, 0)", test_new_val},
	// {"t_node new_val(1, 0)->val", test_new_val},
	// {"t_node new_val(2, 0)->index", test_new_val},

	// // /*  *-*-*-* test_store_in_stack() *-*-*-*  */
	// {"astack->node != NULL", test_store_in_stack},
	// {"astack->size == 7", test_store_in_stack},
	// {"astack->min == 1", test_store_in_stack},
	// {"astack->max == 7", test_store_in_stack},

	// /*  *-*-*-*-*-*-*-*-*-* STACK_OPS.C *-*-*-*-*-*-*-*-*-*  */
	// /*  *-*-*-* test_push() *-*-*-*  */
	// {"\n    Push new_val(3, 0) stack->size == 1", test_push},
	// {"stack->min == 3", test_push},
	// {"stack->max == 3", test_push},
	// {"stack->node->val == 3", test_push},
	// {"stack->node->next == NULL", test_push},
	// {"stack->node->prev == NULL", test_push},

	// {"\n    Push new_val(1, 1) stack->size == 2", test_push},
	// {"stack->min == 1", test_push},
	// {"stack->max == 3", test_push},
	// {"stack->node->val == 1", test_push},
	// {"stack->node->next->val == 3", test_push},
	// {"stack->node->next->prev->val == 1", test_push},
	// {"stack->node->prev == NULL", test_push},
	// {"stack->node->next->next == NULL", test_push},

	// {"\n    Push new_val(7, 2) stack->size == 3", test_push},
	// {"stack->min == 1", test_push},
	// {"stack->max == 7", test_push},
	// {"stack->node->val == 7", test_push},
	// {"stack->node->next->val == 1", test_push},
	// {"stack->node->next->prev->val == 7", test_push},
	// {"stack->node->next->next->val == 3", test_push},
	// {"stack->node->prev == NULL", test_push},

	// /*  *-*-*-* test_pop() *-*-*-*  */
	// {"\n    Pop '5 3 1' stack->size == 3", test_pop},
	// {"stack->min == 1", test_pop},
	// {"stack->max == 5", test_pop},
	// {"stack->node->val == 5", test_pop},
	// {"stack->node->next->val == 3", test_pop},
	// {"stack->node->next->next->val == 1", test_pop},
	// {"stack->node->next->prev->val == 5", test_pop},
	// {"stack->node->prev == NULL", test_pop},
	// {"stack->node->next->next->next == NULL", test_pop},

	// {"\n    Pop '3 1' stack->size == 2", test_pop},
	// {"stack->min == 1", test_pop},
	// {"stack->max == 3", test_pop},
	// {"stack->node->val == 3", test_pop},
	// {"stack->node->next->val == 1", test_pop},
	// {"stack->node->next->prev->val == 3", test_pop},
	// {"stack->node->prev == NULL", test_pop},
	// {"stack->node->next->next-> == NULL", test_pop},

	// {"\n    Pop '1' stack->size == 1", test_pop},
	// {"stack->min == 1", test_pop},
	// {"stack->max == 1", test_pop},
	// {"stack->node->val == 1", test_pop},
	// {"stack->node->next == NULL", test_pop},
	// {"stack->node->prev == NULL", test_pop},

	// {"\n    Pop EMPTY stack->size == 0", test_pop},
	// {"stack->min == 1", test_pop},
	// {"stack->max == 0", test_pop},
	// {"stack->node == NULL", test_pop},

	// /*  *-*-*-* test_last_node() *-*-*-*  */
	// {"\n    Last_node last_node(stack)->val == 1", test_last_node},
	// {"Last_node last_node(stack)->val == 1", test_last_node},

	// /*  *-*-*-*-*-*-*-*-*-* ALLOWED_OPS.C *-*-*-*-*-*-*-*-*-*  */
	// /*  *-*-*-* test_swap_val() *-*-*-*  */
	// {"\n    Push '5 3 1' stack->size == 3", test_swap_val},
	// {"stack->min == 1", test_swap_val},
	// {"stack->max == 5", test_swap_val},
	// {"stack->node->val == 5", test_swap_val},
	// {"stack->node->prev == NULL", test_swap_val},
	// {"stack->node->next->val == 3", test_swap_val},
	// {"stack->node->next->prev->val == 5", test_swap_val},

	// {"\n    Swap'5 3' stack->node->val == 3", test_swap_val},
	// {"stack->node->prev == NULL", test_swap_val},
	// {"stack->node->next->val == 5", test_swap_val},
	// {"stack->node->next->next == NULL", test_swap_val},
	// {"stack->node->next->prev->val == 3", test_swap_val},

	// {"\n    Swap'5 1' stack->node->val == 1", test_swap_val},
	// {"stack->node->prev == NULL", test_swap_val},
	// {"stack->node->next->val == 5", test_swap_val},
	// {"stack->node->next->prev->val == 1", test_swap_val},
	// {"stack->node->next->next == NULL", test_swap_val},

	
	// /*  *-*-*-* test_push_to_stack() *-*-*-*  */
	// {"\n    Push '5 3 1' Astack->size == 3 ", test_push_to_stack},
	// {"astack->min == 1", test_swap_val},
	// {"astack->max == 5", test_swap_val},
	// {"astack->node->val == 5", test_swap_val},
	// {"last_node(astack)->val == 1", test_swap_val},

	// {"\n    Push '5' Astack->size == 2", test_push_to_stack},
	// {"astack->min == 1", test_swap_val},
	// {"astack->max == 3", test_swap_val},
	// {"astack->node->val == 3", test_swap_val},
	// {"last_node(astack)->val == 1", test_swap_val},
	// {"last_node(astack)->prev == NULL", test_swap_val},

	// {"\n    Push '5' Bstack->size == 1", test_push_to_stack},
	// {"bstack->min == 5", test_swap_val},
	// {"bstack->max == 5", test_swap_val},
	// {"bstack->node->val == 5", test_swap_val},
	// {"last_node(bstack)->val == 5", test_swap_val},
	// {"last_node(bstack)->next == NULL", test_swap_val},
	// {"last_node(bstack)->prev == NULL", test_swap_val},

	// {"\n    Push '3' Astack->size == 1", test_push_to_stack},
	// {"astack->min == 1", test_swap_val},
	// {"astack->max == 1", test_swap_val},
	// {"astack->node->val == 1", test_swap_val},
	// {"last_node(astack)->val == 1", test_swap_val},

	// {"\n    Push '3' Bstack->size == 2", test_push_to_stack},
	// {"bstack->min == 3", test_swap_val},
	// {"bstack->max == 5", test_swap_val},
	// {"bstack->node->val == 3", test_swap_val},
	// {"bstack->node->prev == NULL", test_swap_val},
	// {"bstack->node->next->val == 5", test_swap_val},
	// {"last_node(astack)->val == 5", test_swap_val},
	// {"last_node(astack)->prev->val == 3", test_swap_val},
	// {"last_node(astack)->next == NULL", test_swap_val},
	// {"\n    POP Astack == NULL", test_push_to_stack},
	// {"POP Bstack == NULL", test_swap_val},

	// /*  *-*-*-* test_rotate() *-*-*-*  */
	// {"\n    Push '5 3 7 1' Astack->size == 4", test_rotate},
	// {"astack->node->val == 5", test_rotate},
	// {"astack->node->prev == NULL", test_rotate},
	// {"last_node(astack)->val == 1", test_rotate},
	// {"last_node(astack)->prev == NULL", test_rotate},

	// {"\n    rotate '5 3 7 1' Astack->size == 4", test_rotate},
	// {"astack->node->val == 3", test_rotate},
	// {"astack->node->next->val == 7", test_rotate},
	// {"astack->node->next->prev->val == 3", test_rotate},
	// {"astack->node->prev == NULL", test_rotate},
	// {"last_node(astack)->val == 5", test_rotate},
	// {"last_node(astack)->prev->val == 1", test_rotate},
	// {"last_node(astack)->prev->next->val == 5", test_rotate},
	// {"last_node(astack)->next == NULL", test_rotate},

	// {"\n    rotate '5 3 7 1' Astack->size == 4", test_rotate},
	// {"astack->node->val == 7", test_rotate},
	// {"astack->node->next->val == 1", test_rotate},
	// {"astack->node->next->prev->val == 7", test_rotate},
	// {"astack->node->prev == NULL", test_rotate},
	// {"last_node(astack)->val == 3", test_rotate},
	// {"last_node(astack)->prev->val == 5", test_rotate},
	// {"last_node(astack)->prev->next->val == 3", test_rotate},
	// {"last_node(astack)->next == NULL", test_rotate},

	// /*  *-*-*-* test_reverse_rotate() *-*-*-*  */
	// {"\n    Push '5 3 7 1' Astack->size == 4", test_reverse_rotate},
	// {"astack->node->val == 5", test_reverse_rotate},
	// {"astack->node->prev == NULL", test_reverse_rotate},
	// {"last_node(astack)->val == 1", test_reverse_rotate},
	// {"last_node(astack)->prev == NULL", test_reverse_rotate},

	// {"\n    Push '5 3 7 1' Astack->size == 4", test_reverse_rotate},
	// {"astack->node->val == 1", test_reverse_rotate},
	// {"astack->node->next->val == 5", test_reverse_rotate},
	// {"astack->node->next->prev->val == 1", test_reverse_rotate},
	// {"astack->node->prev == NULL", test_reverse_rotate},
	// {"last_node(astack)->val == 7", test_reverse_rotate},
	// {"last_node(astack)->prev->val == 3", test_reverse_rotate},
	// {"last_node(astack)->prev->next->val == 7", test_reverse_rotate},
	// {"last_node(astack)->next == NULL", test_reverse_rotate},

	// {"\n    Push '5 3 7 1' Astack->size == 4", test_reverse_rotate},
	// {"astack->node->val == 7", test_reverse_rotate},
	// {"astack->node->next->val == 1", test_reverse_rotate},
	// {"astack->node->next->prev->val == 7", test_reverse_rotate},
	// {"astack->node->prev == NULL", test_reverse_rotate},
	// {"last_node(astack)->val == 3", test_reverse_rotate},
	// {"last_node(astack)->prev->val == 5", test_reverse_rotate},
	// {"last_node(astack)->prev->next->val == 3", test_reverse_rotate},
	// {"last_node(astack)->next == NULL", test_reverse_rotate},

	// /*  *-*-*-*-*-*-*-*-*-* SOTING_SMALL.C *-*-*-*-*-*-*-*-*-*  */
	// /*  *-*-*-* test_sorting_three() *-*-*-*  */
	// {"\n    Push '2 1 3' Astack->size == 3", test_sorting_three},
	// {"astack->node->val == 2", test_sorting_three},
	// {"astack->node->next->val == 1", test_sorting_three},
	// {"last_node(astack)->val == 3", test_sorting_three},
	// {"\n    3 SORTING '2 1 3' Astack->size == 3", test_sorting_three},
	// {"astack->node->val == 1", test_sorting_three},
	// {"astack->node->next->val == 2", test_sorting_three},
	// {"last_node(astack)->val == 3", test_sorting_three},
	// {"truns <= 3", test_sorting_three},

	// {"\n    Push '3 2 1' Astack->size == 3", test_sorting_three},
	// {"astack->node->val == 3", test_sorting_three},
	// {"astack->node->next->val == 2", test_sorting_three},
	// {"last_node(astack)->val == 1", test_sorting_three},
	// {"\n    3 SORTING '2 1 3' Astack->size == 3", test_sorting_three},
	// {"astack->node->val == 1", test_sorting_three},
	// {"astack->node->next->val == 2", test_sorting_three},
	// {"last_node(astack)->val == 3", test_sorting_three},
	// {"turns <= 3", test_sorting_three},

	// {"\n    Push '3 1 2' Astack->size == 3", test_sorting_three},
	// {"astack->node->val == 3", test_sorting_three},
	// {"astack->node->next->val == 1", test_sorting_three},
	// {"last_node(astack)->val == 2", test_sorting_three},
	// {"\n    3 SORTING '2 1 3' Astack->size == 3", test_sorting_three},
	// {"astack->node->val == 1", test_sorting_three},
	// {"astack->node->next->val == 2", test_sorting_three},
	// {"last_node(astack)->val == 3", test_sorting_three},
	// {"turns <= 3", test_sorting_three},
	
	// {"\n    Push '1 3 2' Astack->size == 3", test_sorting_three},
	// {"astack->node->val == 1", test_sorting_three},
	// {"astack->node->next->val == 3", test_sorting_three},
	// {"last_node(astack)->val == 2", test_sorting_three},
	// {"\n    3 SORTING '2 1 3' Astack->size == 3", test_sorting_three},
	// {"astack->node->val == 1", test_sorting_three},
	// {"astack->node->next->val == 2", test_sorting_three},
	// {"last_node(astack)->val == 3", test_sorting_three},
	// {"turns <= 3", test_sorting_three},

	// {"\n    Push '2 3 1' Astack->size == 3", test_sorting_three},
	// {"astack->node->val == 2", test_sorting_three},
	// {"astack->node->next->val == 3", test_sorting_three},
	// {"last_node(astack)->val == 1", test_sorting_three},
	// {"\n    3 SORTING '2 1 3' Astack->size == 3", test_sorting_three},
	// {"astack->node->val == 1", test_sorting_three},
	// {"astack->node->next->val == 2", test_sorting_three},
	// {"last_node(astack)->val == 3", test_sorting_three},
	// {"turns <= 3", test_sorting_three},

	// {"\n    Push '5 3 2 4 1' Astack->size == 5", test_sorting_five},
	// {"astack->node->val == 5", test_sorting_five},
	// {"astack->node->prev == NULL", test_sorting_five},
	// {"astack->node->next->val == 3", test_sorting_five},
	// {"last_node(astack)->val == 1", test_sorting_five},
	// {"last_node(astack)->next == NULL", test_sorting_five},
	// // push_to_stack(&astack, &bstack, &turns);
	// {"\n    push2stack '5 3 2 4 1' Astack->size == 4", test_sorting_five},
	// {"astack->node->val == 3", test_sorting_five},
	// {"astack->node->prev == NULL", test_sorting_five},
	// {"astack->node->next->val == 2", test_sorting_five},
	// {"last_node(astack)->val == 1", test_sorting_five},
	// {"last_node(astack)->next == NULL", test_sorting_five},

	// // {"\n    Push '5 3 2 4 1' Bstack->size == 1", test_sorting_five},
	// {"bstack->node->val == 5", test_sorting_five},
	// {"bstack->node->prev == NULL", test_sorting_five},

	// // push_to_stack(&astack, &bstack, &turns);
	// {"\n    push2stack '5 3 2 4 1' Astack->size == 3", test_sorting_five},
	// {"astack->node->val == 2", test_sorting_five},
	// {"astack->node->prev == NULL", test_sorting_five},
	// {"astack->node->next->val == 4", test_sorting_five},
	// {"last_node(astack)->val == 1", test_sorting_five},
	// {"last_node(astack)->next == NULL", test_sorting_five},

	// {"\n    Push '5 3 2 4 1' Bstack->size == 2", test_sorting_five},
	// {"bstack->node->val == 3", test_sorting_five},
	// {"bstack->node->prev == NULL", test_sorting_five},
	// {"bstack->node->next->val == 5", test_sorting_five},
	// {"bstack->node->next->next == NULL", test_sorting_five},

	// // sorting_three(&astack, &turns);
	// {"\n    sorting_three '1 2 4' Astack->size == 3", test_sorting_five},
	// {"head->val < head->val", test_sorting_five},
	// {"last->prev->val < last->val", test_sorting_five},

	// // algo_five(&astack, &bstack, &turns);
	// {"\n    algo '1 2 3 4' Bstack->size == 4", test_sorting_five},
	// {"Is Sorted == True", test_sorting_five},

	// // algo_five(&astack, &bstack, &turns);
	// {"\n    algo '1 2 3 4 5' Bstack->size == 4", test_sorting_five},
	// {"Is Sorted == True", test_sorting_five},


	{0, 0},
};








